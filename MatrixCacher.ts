import { Bridge, UserProfile } from "matrix-appservice-bridge";
import { configure } from "matrix-appservice-bridge/lib/components/logging";

interface CachedUserProfile {
    profile: UserProfile
    timestamp: number
}

export class MatrixCacher {
    private matrix: Bridge;
    private config: Record<string, unknown>
    
    private profileCache: Record<string, CachedUserProfile> = {};

    constructor(matrix: Bridge, config: Record<string, unknown>) {
        this.matrix = matrix;
        this.config = config;
    }

    async getUserProfile(userId: string, roomId: string): Promise<UserProfile | null> {
        if (this.profileCache[userId]) {
            if (Date.now() - this.profileCache[userId].timestamp <= (this.config.caching as any).matrixProfileTTL) {
                return this.profileCache[userId].profile;
            }
        }

        const roomState = (await this.matrix.getIntent().roomState(roomId)) as Array<any>
        const member = roomState.find((val) => {
            return val.type == "m.room.member" && val.state_key == userId;
        });

        if (!member) return null;
        const profile: UserProfile = {
            displayname: member.content["displayname"],
            avatar_url: member.content["avatar_url"]
        };
        return profile;
    }
}