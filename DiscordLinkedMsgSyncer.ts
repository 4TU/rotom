import { Client as PGClient } from "pg";

export type LinkedMessageOpts = {
    messageId?: string
    eventId?: string
};

export type DiscordLinkedMessage = {
    channelid: string
    eventid: string
    roomid: string
    messageid: string
};

export class DiscordLinkedMsgSyncer {
    private db: PGClient

    constructor(db: PGClient) {
        this.db = db;
        db.query('CREATE TABLE IF NOT EXISTS discord_linked_messages(channelId VARCHAR(20) NOT NULL, eventId TEXT UNIQUE NOT NULL, roomId TEXT NOT NULL, messageId VARCHAR(20) UNIQUE NOT NULL);');
    }

    async addLinkedMessageEvent(channelId: string, eventId: string, roomId: string, messageId: string) {
        await this.db.query("INSERT INTO discord_linked_messages VALUES ($1, $2, $3, $4);", [channelId, eventId, roomId, messageId]);
    }

    async getLinkedMessage(opts: LinkedMessageOpts) {
        if (opts.eventId && opts.messageId) {
            const res = (await this.db.query("SELECT * FROM discord_linked_messages WHERE eventId = $1 AND messageId = $2 LIMIT 1;", [opts.eventId, opts.messageId])).rows[0];
            if (res) {
                return res as DiscordLinkedMessage;
            }else {
                return undefined;
            }
        }else if (opts.eventId) {
            const res = (await this.db.query("SELECT * FROM discord_linked_messages WHERE eventId = $1 LIMIT 1;", [opts.eventId])).rows[0];
            if (res) {
                return res as DiscordLinkedMessage;
            }else {
                return undefined;
            }
        }else if (opts.messageId) {
            const res = (await this.db.query("SELECT * FROM discord_linked_messages WHERE messageId = $1 LIMIT 1;", [opts.messageId])).rows[0];
            if (res) {
                return res as DiscordLinkedMessage;
            }else {
                return undefined;
            }
        }
    }

    async removeLinkedMessage(opts: LinkedMessageOpts) {
        if (opts.eventId) {
            await this.db.query("DELETE FROM discord_linked_messages WHERE eventId = $1", [opts.eventId]);
        }else if (opts.messageId) {
            await this.db.query("DELETE FROM discord_linked_messages WHERE messageId = $1", [opts.messageId]);
        }
    }
}