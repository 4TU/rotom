/*const Discord = require('discord.js')
const discord = new Discord.Client();

discord.once('ready', () => {
    console.log("Discord connected");
});*/

import { Logging, Cli, Bridge, AppServiceRegistration, MatrixUser } from "matrix-appservice-bridge"
import { Client as PGClient } from "pg"
import { DiscordBridge } from "./DiscordBridge"
import { MatrixCacher } from "./MatrixCacher";

let bridge: Bridge;
let db: PGClient;
let discord: DiscordBridge;
let matrixCache: MatrixCacher;

Logging.configure({
    // A level to set the console reporter to.
    console: "warn",

    // Format to append to log files.
    fileDatePattern: "YYYY-MM-DD",

    // Format of the timestamp in log files.
    timestampFormat: "MMM-D HH:mm:ss.SSS",

    // The maximum number of files per level before old files get cleaned
    // up. Use 0 to disable.
    maxFiles: 2,
});

new Cli({
    registrationPath: "rotom-registration.yaml",
    generateRegistration: function(reg, callback) {
        reg.setId(AppServiceRegistration.generateToken());
        reg.setHomeserverToken(AppServiceRegistration.generateToken());
        reg.setAppServiceToken(AppServiceRegistration.generateToken());
        reg.setSenderLocalpart("rotom");
        reg.addRegexPattern("users", "@rotom_.*", true);
        callback(reg);
    },
    bridgeConfig: {
        schema: "rotom-config-schema.yaml",
        defaults: {}
    },
    run: function(port, config) {
        if (!config) {
            console.error("No config found");
            return
        }

        bridge = new Bridge({
            homeserverUrl: config?.homeserverUrl as string,
            domain: config?.domain as string,
            registration: "rotom-registration.yaml",
            disableStores: true,
            disableContext: true,

            controller: {
                onUserQuery: function(queriedUser) {
                    return {};
                },

                onEvent: function(req, context) {
                    const event = req.getData();
                    switch (event.type) {
                        case 'm.room.message':
                            if (!event.content) {
                                return;
                            }
                            discord.onMatrixMessage(event);
                            break;
                            
                        case 'm.room.encryption':
                            bridge.getIntent().sendMessage(event.room_id, {
                                body: "Encryption was enabled, this room won't be bridged anymore.",
                                msgtype: "m.notice",
                                status: "critical",
                                format: "org.matrix.custom.html",
                                formatted_body: "<b>Encryption was enabled, this room won't be bridged anymore.</b>"
                            }).then(id => {
                                discord.roomReset(event.room_id);
                                bridge.getIntent().leave(event.room_id);
                            });
                            break;
                        case 'm.room.redaction':
                            if (!event.redacts) return;
                            discord.onMatrixRedact(event);
                            break;

                        default:
                            console.log("Unhandled matrix event: " + event.type);
                            break;
                    }
                }
            },
            intentOptions: {
                clients: {
                    dontJoin: true
                }
            }
        });

        const pgconf = config?.postgres as any
        if (!pgconf) {
            console.error("Warning: No postgres config set");
        }
        db = new PGClient({
            user:       pgconf['user'] ? pgconf['user'] : process.env.USER,
            password:   pgconf['password'] ? pgconf['password'] : null,
            host:       pgconf['host'] ? pgconf['host'] : 'localhost',
            port:       pgconf['port'] ? pgconf['port'] : 5432,
            database:   pgconf['database']? pgconf['database'] : process.env.USER
        });

        db.connect(async (e: Error) => {
            if (e) {
                console.error("Postgres error:\n", e.stack);
            }else {
                console.log("Postgres connected");
                await bridge.run(port, config, undefined, config?.hostname ? config?.hostname as string : undefined, 1);
                console.log("Matrix connected");
                matrixCache = new MatrixCacher(bridge, config);

                try {
                    await bridge.provisionUser(new MatrixUser(`@rotom:${config.domain}`), {
                        name: "Rotom [BOT]",
                        url: "https://cdn.discordapp.com/avatars/776986822634766347/bf2add31ab10a43e5221febed55720e7.png"
                    });
                } catch (e) {
                    if (e.name != "M_USER_IN_USE") throw e;
                }

                /* === Discord === */
                discord = new DiscordBridge(bridge, config, db, matrixCache);
                await discord.login();
            }
        });
    }
}).run();