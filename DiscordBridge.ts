import { Client as DiscordClient, Guild, GuildChannel, Message, MessageEmbed, PartialMessage, TextChannel, Webhook} from "discord.js";
import { Bridge, Intent, MatrixUser, Request, WeakEvent } from "matrix-appservice-bridge";
import { format } from "util";
import { Client as PGClient } from "pg";
import { MatrixCacher } from "./MatrixCacher";
import { DiscordCacher } from "./DiscordCacher";
import * as request from "request";
import { DiscordLinkedMessage, DiscordLinkedMsgSyncer, LinkedMessageOpts } from "./DiscordLinkedMsgSyncer";
import { DiscordMessageFormatter } from "./DiscordMessageFormatter";

export class DiscordBridge {
    private discord: DiscordClient;
    private matrix: Bridge;
    private config: Record<string, unknown>;
    private db: PGClient;
    private matrixCache: MatrixCacher
    private discordCache: DiscordCacher
    private msgSyncer: DiscordLinkedMsgSyncer
    private msgFormatter: DiscordMessageFormatter

    constructor(matrix: Bridge, config: Record<string, unknown>, db: PGClient, matrixCache: MatrixCacher) {
        this.discord = new DiscordClient();
        this.matrix = matrix;
        this.config = config;
        this.db = db;
        this.matrixCache = matrixCache;
        this.discordCache = new DiscordCacher(db, this.discord);
        this.msgSyncer = new DiscordLinkedMsgSyncer(db);
        this.msgFormatter = new DiscordMessageFormatter(this.discordCache, matrixCache, matrix);

        db.query('CREATE TABLE IF NOT EXISTS channel_associations(discord VARCHAR(20) NOT NULL UNIQUE, matrix TEXT NOT NULL UNIQUE);');
        db.query('CREATE TABLE IF NOT EXISTS discord_server(id VARCHAR(20) NOT NULL UNIQUE);');
        db.query('CREATE TABLE IF NOT EXISTS account_associations(discord VARCHAR(20) NOT NULL UNIQUE, matrix TEXT NOT NULL UNIQUE);');

        this.discord.once('ready', () => {
            console.log("Discord connected");
        });

        this.discord.on('message', async (msg) => {
            if (!msg.guild) return;
            if (msg.author.id == this.discord.user?.id) return;
            if (msg.webhookID && this.isMatrixWebhookMsg(msg)) {
                return;
            }

            if (msg.content &&
                msg.content.startsWith((this.config.discord as any).prefix ? (this.config.discord as any).prefix as string : "&") &&
                msg.guild?.member(msg.author)?.hasPermission('MANAGE_GUILD')
                && !msg.webhookID) {
                await this.handleCommand(msg);
            }else {
                const roomId = await this.discordCache.matrixFromDiscord(msg.channel.id);
                if (roomId == null) return;
                const mUsername = format("@rotom_dcord_%s:%s", msg.author.id, this.config.domain);

                const avatarURL = msg.author.avatarURL({dynamic: false, format: "png", size: 128});
                const nickname = msg.guild.member(msg.author)?.nickname ? msg.guild.member(msg.author)?.nickname + "#" + msg.author.discriminator : msg.author.username + "#" + msg.author.discriminator;
                try {
                    if (!(await this.discordCache.getAccount({matrix: mUsername}))) {
                        await matrix.provisionUser(new MatrixUser(mUsername), {
                            name: nickname ? nickname : msg.author.username,
                            url: avatarURL ? avatarURL : undefined
                        });
                        this.discordCache.addAccount(msg.author.id, mUsername);
                    }
                } catch (e) {
                    if (e.name != "M_USER_IN_USE") throw e;
                }
                const intent = this.matrix.getIntent(mUsername);
                if (avatarURL) await intent.setAvatarUrl(avatarURL);
                if (nickname) await intent.setDisplayName(nickname);

                await this.joinRoom(intent, roomId);

                const formattedMessage = await this.msgFormatter.discordToMatrix(msg, roomId);
                const html = formattedMessage.body != formattedMessage.formatted_body;
                const { event_id } = await intent.sendMessage(roomId, {
                    "msgtype": "m.text",
                    "format": html ? "org.matrix.custom.html" : undefined,
                    "body": formattedMessage.body,
                    "formatted_body": html ? formattedMessage.formatted_body : undefined,
                    "rotom.discord_id": msg.id
                });
                await this.msgSyncer.addLinkedMessageEvent(msg.channel.id, event_id, roomId, msg.id);
            }
        });

        this.discord.on('messageUpdate', async (oldMsg, msg) => {
            if (!msg.guild) return;
            if (msg.author?.id == this.discord.user?.id) return;
            if (msg.webhookID && this.isMatrixWebhookMsg(msg)) {
                return;
            }
            
            const linkedMsg = await this.msgSyncer.getLinkedMessage({messageId: msg.id});
            if (linkedMsg) {
                const event = await matrix.getIntent().getEvent(linkedMsg.roomid, linkedMsg.eventid);
                if (event["rotom.discord_id"]) {
                    if (event["rotom.discord_id"] != linkedMsg.messageid) {
                        console.error("Discord id didn't match linked id, clearing link");
                        this.msgSyncer.removeLinkedMessage({messageId: linkedMsg.messageid})
                        return;
                    }
                }
                const formattedMessage = await this.msgFormatter.discordToMatrix(msg as Message, linkedMsg.roomid);
                const html = formattedMessage.body != formattedMessage.formatted_body;
                await matrix.getIntent(event.sender).sendMessage(linkedMsg.roomid, {
                    "m.new_content": {
                        "msgtype": "m.text",
                        "format": html ? "org.matrix.custom.html" : undefined,
                        "body": formattedMessage.body,
                        "formatted_body": html ? formattedMessage.formatted_body : undefined,
                        "rotom.discord_id": msg.id
                    },
                    "m.relates_to": {
                        "rel_type": "m.replace",
                        "event_id": linkedMsg.eventid
                    },
                    "msgtype": "m.text",
                    "body": ` * ${msg.content}`
                });
            }
        });

        this.discord.on('messageDelete', async (msg) => {
            if (!msg.guild) return;
            if (msg.author?.id == this.discord.user?.id) return;

            const linkedMsg = await this.msgSyncer.getLinkedMessage({messageId: msg.id});
            if (linkedMsg) {
                const event = await matrix.getIntent().getEvent(linkedMsg.roomid, linkedMsg.eventid);
                if (event["rotom.discord_id"]) {
                    if (event["rotom.discord_id"] != linkedMsg.messageid) {
                        console.error("Discord id didn't match linked id, clearing link");
                        this.msgSyncer.removeLinkedMessage({messageId: linkedMsg.messageid})
                        return;
                    }
                }
                if (!event.sender.startsWith("@rotom_dcord")) event.sender = undefined; // Dont try to pretend to be a real user because it won't work
                try {
                    const redactEvent = await matrix.getIntent(event.sender).getClient().redactEvent(linkedMsg.roomid, linkedMsg.eventid);
                    if (redactEvent) this.msgSyncer.removeLinkedMessage({messageId: linkedMsg.messageid});
                }catch (e) {
                    if (e.name == "M_FORBIDDEN") {
                        console.error("Tried to redact but don't have sufficient priviledges on Matrix");
                    }else {
                        throw e;
                    }
                }
            }
        });
    }

    roomReset(roomId: string) {
        this.db.query("DELETE FROM channel_associations WHERE matrix = $1", [roomId]);
    }

    private async isMatrixWebhookMsg(msg: Message | PartialMessage) {
        return (await msg.guild?.fetchWebhooks())?.find((v) => {return v.name == "_matrix" && v.channelID == msg.channel.id;})?.id == msg.webhookID;
    }

    private async handleCommand(msg: Message) {
        const command = msg.content.substr(1).split(" ");
                switch (command[0]) {
                    case "roomset":
                        if (command[1] && command.length <= 2) {
                            if (!msg.guild) return;
                            this.discordCache.setGuild(msg.guild.id);
                            try {
                                const bot = this.matrix.getIntent();
                                const roomId = await this.joinRoom(bot, command[1]);
                                const hook = this.discordCache.ensureHook(msg.channel as TextChannel);

                                this.db.query('INSERT INTO channel_associations VALUES ($1, $2)', [msg.channel.id, roomId], (e, res) => {
                                    if (e) {
                                        console.log(`Failed to associate room:\n${e.stack}`);
                                        msg.channel.send("Room already has an association");
                                    }else {
                                        msg.channel.send(`Channel successfully associated with \`${command[1]}\``)
                                        bot.sendMessage(roomId, {
                                            body: `This room is now associated with #${(msg.channel as TextChannel).name} on Discord`,
                                            msgtype: "m.notice",
                                            format: "org.matrix.custom.html",
                                            formatted_body: `<b>This room is now associated with <code>#${(msg.channel as TextChannel).name}</code> on Discord</b>`
                                        });
                                    }
                                })
                            } catch(e) {
                                console.log(`Failed to associate room:\n${e.stack}`);
                                msg.channel.send("Room doesn't exist or other error");
                            }
                        }else {
                            msg.channel.send("Expected 1 argument (public room ID in format #room:server)")
                        }
                        break;
                    
                    case "roomreset":
                        try {
                            const old = await this.discordCache.matrixFromDiscord(msg.channel.id);
                            if (old == null) {
                                msg.channel.send("Couldn't reset room association");
                                return;
                            }

                            await this.db.query('DELETE FROM channel_associations WHERE discord = $1', [msg.channel.id]);
                            msg.channel.send("Successfully reset room association");
                            this.matrix.getIntent().sendMessage(old, {
                                body: `This room is no longer associated with a Discord channel`,
                                msgtype: "m.notice",
                                format: "org.matrix.custom.html",
                                formatted_body: `<b>This room is no longer associated with a Discord channel</b>`
                            });
                        } catch(e) {
                            console.log(`Failed to reset room:\n${e.stack}`);
                            msg.channel.send("Couldn't reset room association");
                        }
                        break;
                }
    }

    private async joinRoom(bot: Intent, room: string): Promise<string> {
        const opts: { syncRoom: boolean, viaServers?: string[] } = {
            syncRoom: false,
        };
        const { roomId } = await bot.getClient().joinRoom(room, opts);
        return roomId as string
    }

    async login() {
        await this.discord.login((this.config.discord as any).token)
    }

    async onMatrixMessage(message: WeakEvent) {
        if (message.sender.startsWith("@rotom_dcord")) return;

        const channelId = await this.discordCache.discordFromMatrix(message.room_id);
        if (channelId == null) return;
        
        const hook = await this.discordCache.getOrCreateHook(channelId);
        const profile = await this.matrixCache.getUserProfile(message.sender, message.room_id);

        const headers = {'Content-Type': 'application/json'};
        let synced: undefined | DiscordLinkedMessage = undefined;
        if (message.content["m.new_content"] && message.content["m.relates_to"]) {
            const related_id = (message.content["m.relates_to"] as any)["event_id"];
            if (related_id) {
                synced = await this.msgSyncer.getLinkedMessage({eventId: related_id});
                console.log(JSON.stringify(synced));
            }
        }
        if (synced) {
            const reqBody = {
                "content": (message.content["m.new_content"] as any).body
            };
            request.patch(hook + "/messages/" + synced.messageid, {
                headers: headers,
                body: JSON.stringify(reqBody)
            }, (e, res, body) => {
                if (e) {
                    console.error("Failed to forward message edit from matrix (discord webhook)\n:", e);
                }else {
                    if (res.statusCode.toString()[0] != "2") {
                        console.error(`Error status code forwarding message edit from matrix to discord webhook (${res.statusCode}):\n${body}`);
                    }
                }
            });
        }else {
            let reqBody = {
                "content": message.content.body,
                "username": profile?.displayname ? profile?.displayname : message.sender,
            };
            request.post(hook + "?wait=true", {
                headers: headers,
                body: JSON.stringify(reqBody)
            }, (e, res, body) => {
                const jsonBody = JSON.parse(body);
                if (e) {
                    console.error("Failed to forward message from matrix (discord webhook)\n:", e);
                }else {
                    if (res.statusCode.toString()[0] != "2") {
                        console.error(`Error status code forwarding message from matrix to discord webhook (${res.statusCode}):\n${body}`);
                    }else {
                        this.msgSyncer.addLinkedMessageEvent(channelId, message.event_id, message.room_id, jsonBody.id);
                    }
                }
            });
        }
        //if (profile?.avatar_url) reqBody["avatar_url"] = this.matrix.getIntent().getClient().mxcToHttp(profile?.avatar_url);
    }

    async onMatrixRedact(event: WeakEvent) {
        const channelId = await this.discordCache.discordFromMatrix(event.room_id);
        if (channelId == null) return;

        const linkedMsg = await this.msgSyncer.getLinkedMessage({eventId: (event as any).redacts});
        if (linkedMsg) {
            const channel = (await this.discord.guilds.fetch(await this.discordCache.getGuild())).channels.resolve(channelId) as TextChannel;
            (await channel.messages.fetch(linkedMsg.messageid)).delete({reason: "Message deleted on Matrix."});
        }
    }
}
