import { TextChannel, Client as DiscordClient } from "discord.js";
import { Client as PGClient } from "pg";

export type LinkedAccount = {
    discord?: string
    matrix?: string
};

export class DiscordCacher {
    private db: PGClient;
    private guildId: string | undefined;
    private discord: DiscordClient;
    private channelCache: Record<string, TextChannel> = {};

    constructor(db: PGClient, discord: DiscordClient) {
        this.db = db;
        this.discord = discord;
        db.query("CREATE TABLE IF NOT EXISTS webhook_cache (channelId VARCHAR(20) NOT NULL UNIQUE, url TEXT NOT NULL UNIQUE);")
    }

    async ensureHook(channel: TextChannel): Promise<string> {
        const webHooks = await channel.fetchWebhooks()
        const cHook = webHooks.find((v) => {
            return v.name == "_matrix" && v.channelID == channel.id;
        });
        if (cHook) return cHook.url;

        const nHook = await channel.createWebhook("_matrix", {reason: "Used for Matrix bridging"});
        return nHook.url;
    }

    async getOrCreateHook(channelId: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.db.query("SELECT * FROM webhook_cache WHERE channelId = $1;", [channelId], async (e, res) => {
                if (e) {
                    throw e;
                }else {
                    if (res.rowCount < 1) {
                        resolve(await this.ensureHook((await (await this.discord.guilds.fetch(await this.getGuild())).channels.resolve(channelId)) as TextChannel));
                    }else {
                        resolve(res.rows[0]["url"]);
                    }
                }
            });
        })
    }

    async removeHook(channelId: string) {
        await this.db.query("DELETE FROM webhook_cache WHERE channelId = $1", [channelId]);
    }

    async setGuild(guildId: string) {
        this.guildId = guildId;
        await this.db.query("INSERT INTO discord_server VALUES ($1) ON CONFLICT (id) DO UPDATE SET id = $1;", [guildId]);
    }

    async getGuild(): Promise<string> {
        if (!this.guildId) {
            const res = await this.db.query("SELECT * FROM discord_server");
            this.guildId = res.rows[0]["id"];
            return res.rows[0]["id"];
        }else {
            return this.guildId;
        }
    }

    async addAccount(discordId: string, matrixId: string) {
        try {
            await this.db.query("INSERT INTO account_associations VALUES ($1, $2);", [discordId, matrixId]);
        } catch (e) {
            if (e.code != 23505) throw e; // Ignore unique constraint violation
        }
    }

    async getAccount(opts: LinkedAccount): Promise<LinkedAccount | undefined> {
        if (opts.discord) {
            const res = await (await this.db.query("SELECT * FROM account_associations WHERE discord = $1 LIMIT 1;", [opts.discord])).rows[0];
            if (res) return res as LinkedAccount; else return undefined;
        }else if (opts.matrix){
            const res = await (await this.db.query("SELECT * FROM account_associations WHERE matrix = $1 LIMIT 1;", [opts.matrix])).rows[0];
            if (res) return res as LinkedAccount; else return undefined;
        }else {
            return undefined;
        }
    }

    async matrixFromDiscord(channelId: string): Promise<string | null> {
        const res = await this.db.query("SELECT * FROM channel_associations WHERE discord = $1;", [channelId]);
        if (res.rowCount == 0) return null;
        return res.rows[0]["matrix"];
    }

    async discordFromMatrix(roomId: string): Promise<string | null> {
        const res = await this.db.query("SELECT * FROM channel_associations WHERE matrix = $1;", [roomId]);
        if (res.rowCount == 0) return null;
        return res.rows[0]["discord"];
    }

    async channelLookup(channelId: string): Promise<TextChannel | null> {
        if (this.channelCache[channelId]) return this.channelCache[channelId];

        const channel = (await this.discord.guilds.fetch(await this.getGuild())).channels.resolve(channelId);
        if (channel) {
            this.channelCache[channelId] = channel as TextChannel;
            return channel as TextChannel;
        } else return null;
    }
};