# Rotom

Extendable matrix bridge currently supporting Discord

## Build
1. `npm install`
2. `npm run tsc`

## Deploy
1. If deploying somewhere else:
     1. `npm remove typescript`
     2. copy `package-lock.json`, `package.json`, `build`, `node_modules` to target
2. `node . -r -u http://(binding address / localhost):6789`
3. `cp template-config.yaml config.yaml`
4. Edit config as needed

### Setup (Discord)
1. Ensure `config.yaml` has discord bot token
2. Add bot to server: `https://discord.com/oauth2/authorize?client_id=(YOUR BOT CLIENT ID)&scope=bot&permissions=536964160`
3. You can now use commands to set up the bridge:  
    - `&roomset (matrix room ID)` to bridge a channel to a matrix room
    - `&roomreset` to remove the bridge

### Run
 - `node . -c config.yaml`