import { Client, Message } from "discord.js";
import { Bridge, WeakEvent } from "matrix-appservice-bridge";
import { Converter } from "showdown";
import { DiscordCacher } from "./DiscordCacher";
import { MatrixCacher } from "./MatrixCacher";

export type MatrixMessageBody = {
    body: string
    formatted_body: string
}

export class DiscordMessageFormatter {
    private discordCache: DiscordCacher
    private matrixCache: MatrixCacher
    private matrix: Bridge
    private showdown = new Converter({strikethrough: true, underline: true, tables: true, tasklists: true, openLinksInNewWindow: true});

    constructor(discordCache: DiscordCacher, matrixCache: MatrixCacher, matrix: Bridge) {
        this.discordCache = discordCache;
        this.matrixCache = matrixCache;
        this.matrix = matrix
    }
    
    async matrixToDiscord(msg: WeakEvent): Promise<string> {
        let content = msg.content.body as string;
        return content;
    }

    async discordToMatrix(msg: Message, targetRoomId: string): Promise<MatrixMessageBody> {
        let content = msg.content;
        let htmlContent = this.showdown.makeHtml(msg.content).replace(/^<p>|<\/p>$/g, '');

        let done: Record<string, boolean> = {};
        const mentionExp = /<@![0-9]+>/g;

        let mentionRes: RegExpExecArray | null;
        while ((mentionRes = mentionExp.exec(content)) != null) {
            const mention = mentionRes[0];
            if (done[mention]) continue;
            done[mention] = true;

            const id = mention.substr(3, mention.length-4);
            const acc = await this.discordCache.getAccount({discord: id});
            if (acc && acc.matrix) {
                const user = await this.matrixCache.getUserProfile(acc.matrix, targetRoomId);
                if (user) {
                    content = content.replace(mention, user.displayname ? user.displayname : id);
                    htmlContent = htmlContent.replace(mention, `<a href="https://matrix.to/#/${encodeURI(acc.matrix)}">${user.displayname ? user.displayname : id}</a>`);
                }
            }
        }


        done = {};
        const channelExp = /<#[0-9]+>/g;
        let channelRes: RegExpExecArray | null;
        while ((channelRes = channelExp.exec(content)) != null) {
            const channel = channelRes[0];
            if (done[channel]) continue;
            done[channel] = true;

            const id = channel.substr(2, channel.length-3);
            const room = await this.discordCache.matrixFromDiscord(id);
            if (room) {
                const roomAlias = ((await this.matrix.getIntent().roomState(room)) as Array<any>).find(evt => {return evt.type == "m.room.canonical_alias";}).content.alias;
                content = content.replace(channel, roomAlias ? roomAlias : room);

                const channelName = (await this.discordCache.channelLookup(id))?.name;
                htmlContent = htmlContent.replace(channel, `<a href="https://matrix.to/#/${encodeURI(roomAlias ? roomAlias : room)}">${channelName ? "#" + channelName : (roomAlias ? roomAlias : room)}</a>`);
            }
        }
        return {body: content, formatted_body: htmlContent};
    }
}